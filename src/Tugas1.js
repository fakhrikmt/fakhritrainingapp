import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Tugas1 = () => {
    return (
        <View style={styles.container}>
            <Text style={{textAlign: 'center'}}>Kamu Saja</Text>
        </View>
    )
}

export default Tugas1

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#F5F5F5'
    }
})
