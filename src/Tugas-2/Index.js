import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, Image } from 'react-native'
import { Header, Icon } from 'react-native-elements'
import { responsiveHeight, responsiveWidth, responsiveFontSize, responsiveScreenWidth, responsiveScreenHeight, responsiveScreenFontSize, } from 'react-native-responsive-dimensions'

const Index = () => {
    return (
        <SafeAreaView style={styles.container}>

            <Header
                centerComponent={
                    { text: 'Account', 
                    style: { 
                        color: '#fff', 
                        fontSize: responsiveFontSize(2.5), 
                        fontWeight: 'bold', 
                        width: responsiveWidth(90)
                    }     
                }}
            />

            <View style={styles.fotoprofile}>
                <View style={styles.foto}>
                    <Image source={require('./profil.png')} style={styles.fotoprofil} />
                </View>
                <View style={{justifyContent: 'center', width: responsiveWidth(80)}}>
                    <Text style={styles.teksfoto}>La Jupriadi Fakhri</Text>
                </View>
            </View>
            
            
            <View style={styles.saldo}>
                <View style={styles.kotakicon}>
                    <Image source={require('./wallet.png')} style={styles.fotoikon} />
                </View>
                <View style={styles.kotaksaldo}>
                    <View style={styles.namasaldo}>
                        <Text>Saldo</Text>
                    </View>
                    <View style={styles.isisaldo}>
                        <Text style={{textAlign: 'right', paddingRight: 20}}>Rp. 120.000.000</Text>
                    </View>     
                </View>
            </View>

            <View style={{marginBottom: 5}}></View>

            <View style={styles.tigakotak}>
                <View style={styles.bagianikon}>
                    <Image source={require('./setting.png')} style={styles.fotoikon} />
                </View>
                <View style={styles.bagianteks}>
                    <Text>Pengaturan</Text>
                </View>
            </View>
            
            <View style={styles.tigakotak}>
                <View style={styles.bagianikon}>
                    <Image source={require('./help.png')} style={styles.fotoikon1} />
                </View>
                <View style={styles.bagianteks}>
                    <Text>Bantuan</Text>
                </View>
            </View>
            
            <View style={styles.tigakotak}>
                <View style={styles.bagianikon}>
                    <Image source={require('./document.png')} style={styles.fotoikon1} />
                </View>
                <View style={styles.bagianteks}>
                    <Text>Syarat {'&'} Ketentuan</Text>
                </View>
            </View>

            <View style={styles.keluar}>
                <View style={styles.bagianikon}>
                    <Image source={require('./signout.png')} style={styles.fotoikon1} />
                </View>
                <View style={styles.bagianteks}>
                    <Text>Keluar</Text>
                </View>
            </View>

        </SafeAreaView>
    )
}

export default Index

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    foto: {
        width: responsiveWidth(20), 
        backgroundColor:'#fff', 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    fotoprofil: {
        width: 50,
        height: 50,
        justifyContent: 'center'
    },
    fotoikon: {
        width: 30,
        height: 30,
        justifyContent: 'center'
    },
    fotoikon1: {
        width: 25,
        height: 25,
        justifyContent: 'center'
    },
    fotoprofile: {
        backgroundColor: '#fff',
        width: responsiveWidth(100),
        height: responsiveHeight(12),
        marginBottom: 2,
        flexDirection: 'row'
    },
    teksfoto: {
        color: '#000',
        fontSize : responsiveScreenFontSize(2.1),
        fontWeight: 'bold'
    },
    saldo: {
        backgroundColor: '#fff',
        width: responsiveWidth(100),
        height: responsiveHeight(8),
        justifyContent: 'center',
        marginBottom: 4
    },
    tigakotak: {
        backgroundColor: '#fff',
        width: responsiveWidth(100),
        height: responsiveHeight(8),
        marginBottom: 1,
        flexDirection: 'row'
    },
    keluar: {
        backgroundColor: '#fff',
        width: responsiveWidth(100),
        height: responsiveHeight(8),
        justifyContent: 'center',
        marginTop: 4,
        flexDirection: 'row'
    },
    saldo: {
        flexDirection: 'row'
    },
    kotakicon: {
        backgroundColor: '#fff',
        width: responsiveWidth(15),
        height: responsiveHeight(8),
        alignItems: 'center',
        justifyContent: 'center'
    },
    kotaksaldo: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center'
    },
    namasaldo: {
        backgroundColor: '#fff',
        width: responsiveWidth(25)
    },
    isisaldo: {
        backgroundColor: '#fff',
        width: responsiveWidth(60),
    },
    bagianikon: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        width: responsiveWidth(15),
        alignItems: 'center'
    },
    bagianteks: {
        backgroundColor: '#fff',
        width: responsiveWidth(85),
        justifyContent: 'center'
    }
})
